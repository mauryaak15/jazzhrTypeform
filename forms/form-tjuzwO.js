const fieldMap = {
    BciSTRGPVtTW: "email",
    TUYKPtD1BGnJ: "start",
    jdSvFbU06KEE: "about",
    OBK5NWl0NF2o: "currentJobDetails",
    OvPQD7OU7ygf: "currentCTC",
    qBCVTlSn2g3A: "expectedCTC",
    NDYeRyewFsbn: "last_name",
    nvbQ3LfWxRKW: "phone",
    cvhwn3tBPoYc: "file_url",
    DVNU4Ifym5Ww: "currentLocation",
    myw94wETIc0w: "first_name",
    vrgMsU1HKGwy: "dob",
    zBizuKAMkoYb: "qualification",
    VtZohjJ8pnLt: "reference",
    saenTxMlEIFV: "availability"
}

module.exports = fieldMap;