const express = require('express');
const bodyParser = require('body-parser');
const rp = require('request-promise');
const fs = require('fs');

const app = express();

const port = process.env.PORT || 8000;
const ip = '127.0.0.1';
const jazzApiKey = process.env.JZK;
const questionnaireId = 'questionnaire_20171128073006_5MYYZHYMOR7SCFIV';
const source = 'Typeform';
const baseUrl = 'https://api.resumatorapi.com/v1';
let file_url = "";

let jazzFields = { };

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

//routes

app.get('/', (req, res, next) => {
    // storeInJazz().then((data) => {
    //     res.json({status: 200, message: "Success", data: data});
    // })
    // .catch((err) => {
    //     res.json({status: 200, error: err});
    // });
    res.json({status: 200, message: "Success"});
});

app.post('/form/:id', (req, res, next) => {
    const paramFormId = req.params.id;
    console.log(paramFormId," paramformid");
    const formRes = req.body.form_response;
    const formId = formRes.form_id;
    console.log(formId," formid");
    if(paramFormId == formId) {
        const filename = `./forms/form-${formId}`;
        if(fs.existsSync(`${filename}.js`)) {
            const answers = formRes.answers;
            jazzFields.job = formRes.hidden.job_id;
            const formKeyNameObj = require(filename);
            answers.forEach((answer) => {
                let fieldId = answer.field.id;
                let key = formKeyNameObj[fieldId];
                console.log(key, " key");
                if(key === "dob"){
                    if(answer.date === "" || answer.date === undefined || answer.date === null) jazzFields[key]="1600-02-04";
                    jazzFields[key] = answer.date;
                }else if(key === "file_url") {
                    if(answer.file_url === "" || answer.file_url === undefined || answer.file_url === null) jazzFields[key]="/";
                    jazzFields[key] = answer.file_url;
                }else if(key === "phone" || key === "currentCTC" || key === "expectedCTC") {
                    if(answer.number === "" || answer.number === undefined || answer.number === null) jazzFields[key]=9999999999;
                    jazzFields[key] = answer.number;
                }else if(key === "email") {
                    if(answer.email === "" || answer.email === undefined || answer.email === null) jazzFields[key]="def@def.com";
                    jazzFields[key] = answer.email;
                }else {
                    if(answer.text === "" || answer.text === undefined || answer.text === null) jazzFields[key]="default";
                    jazzFields[key] = answer.text;
                }
                
            });
            rp({uri: jazzFields.file_url, encoding: null}).then((resumeBuffer) => {
                const resumeBase64 = new Buffer(resumeBuffer).toString('base64');
                //creating applicant in jazz
                storeApplicantInJazz(resumeBase64).then((id) => {
                    const applicant_id = id;
                    console.log("applicant_id", id);
                    storeQuestionnaireInJazz(applicant_id).then((result) => {
                        console.log('questionnaire posting success', JSON.stringify(result));
                    })
                    .catch((err) => {
                        console.log('Error occurred while posting quesstionaire', err);
                    });
                })
                .catch((err) => {
                    console.log("Error posting applicant", err);
                });  
            })
            .catch((err) => {
                console.log('Error resume fetching: ', err);
            });
        }else {
            console.log('form file not found on server');
        }
    }else {
        console.log('route form id does not match');
    }
        
    res.status(200);
    res.end();
});

//create applicant
function storeApplicantInJazz(resumeBase64) {
    const options = {
        method: 'POST',
        uri: `${baseUrl}/applicants`,
        json: {
            apikey: jazzApiKey,
            first_name: jazzFields.first_name,
            last_name: jazzFields.last_name,
            source: source,
            email: jazzFields.email,
            'base64-resume': resumeBase64,
            phone: jazzFields.phone,
            job: jazzFields.job
        },
    };
    return rp(options).then((res) => {
        return res.prospect_id;
    })
    .catch((err) => {
        return err;
        console.log('Error', err);
    });
    
}

//create applicant
function storeQuestionnaireInJazz(applicant_id) {
    const options = {
        method: 'POST',
        uri: `${baseUrl}/questionnaire_answers`,
        json: {
            apikey: jazzApiKey,
            applicant_id: applicant_id,
            questionnaire_id: questionnaireId,
            answer_value_01: jazzFields.dob,
            answer_value_02: jazzFields.qualification,
            answer_value_03: jazzFields.currentLocation,
            answer_value_04: jazzFields.about,
            answer_value_05: jazzFields.reference,
            answer_value_06: jazzFields.availability,
            answer_value_07: jazzFields.currentJobDetails,
            answer_value_08: jazzFields.start,
            answer_value_09: jazzFields.currentCTC,
            answer_value_10: jazzFields.expectedCTC
            // answer_value_11: jazzFields.comments
        },
    };
    return rp(options).then((res) => {
        return res;
    })
    .catch((err) => {
        return err;
        console.log('Error', err);
    });
    
}


app.listen(port, (err) =>{
    if(err) throw new Error(err);
    else console.log(`Server running on ${ip}:${port}`);
});